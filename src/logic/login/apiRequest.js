const apiRequest = async (requestBody) => {
  if (!requestBody) throw new Error('Argument is required');
  const url = 'https://zwzt-zadanie.netlify.app/api/login';
  try {
    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(requestBody),
    });

    return await response.json();
  } catch (error) {
    throw new Error(error);
  }
};

export default apiRequest;
