import apiRequest from './apiRequest';
import { INVALID_USER, CONNECTION_ERROR_RESPONSE } from './loginResponseObj';

export const login = async (userData) => {
  if (!userData) throw new Error('Argument is required.');
  try {
    const response = await apiRequest(userData);
    if (response.error) return JSON.parse(JSON.stringify(INVALID_USER));
    // saving received token to LocalStorage for future authentication
    const json = JSON.stringify(response.token);
    localStorage.setItem('token', json);
    location.hash = '/success';
  } catch (ereror) {
    // if unable to reach api return error. App,js displays modal with error.
    return CONNECTION_ERROR_RESPONSE;
  }
};

// removes token and redirects to home
export const logout = () => {
  localStorage.removeItem('token');
  location.hash = '';
};

// Username and password validation
export const validateUser = (username, password) => {
  const validation = { username: !username, password: !password };
  let isProcessing = true;
  if (!username || !password) isProcessing = false;

  return { isProcessing, errors: { main: '', validation } };
};
