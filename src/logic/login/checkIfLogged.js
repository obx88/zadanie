// It checks if user is logged, If not it redirects to login panel and saves an error
export default () => {
  try {
    const json = localStorage.getItem('token');
    const token = JSON.parse(json);
    if (token) {
      // Mock "if". Veryfying token here. If token is valid then user has an access
    } else {
      const msg = JSON.stringify('Najpierw musisz się zalogować.');
      localStorage.setItem('redirectMsg', msg);
      location.hash = '';
    }
  } catch {
    location.hash = '';
  }
};
