const validation = { password: false, username: false };

// Object used when api response says, that user input doesn't match any account
export const INVALID_USER = {
  isProcessing: false,
  username: '',
  password: '',
  errors: {
    main: 'Niepoprawna nazwa użytkownika lub hasło.',
    validation,
  },
};

// Object used while trying to login when unable to establish connection with api
export const CONNECTION_ERROR_RESPONSE = {
  errors: {
    main: 'Nie udało się połączyć z serwerem. Spróbuj ponownie.',
    validation,
  },
};
