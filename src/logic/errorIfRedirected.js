export default () => {
  try {
    const json = localStorage.getItem('redirectMsg');
    localStorage.removeItem('redirectMsg');
    const error = JSON.parse(json);
    return error;
  } catch (error) {
    throw new Error(error);
  }
};
