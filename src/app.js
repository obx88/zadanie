import 'normalize.css';
import './styles/styles.scss';
import { route } from './router';
import { login, logout, validateUser } from './logic/login/loginLogic';
import checkIfLogged from './logic/login/checkIfLogged';
import errorIfRedirected from './logic/errorIfRedirected';
import home from './partials/home.html';
import success from './partials/success.html';
import e404 from './partials/404.html';

route('/', home, function () {
  this.isProcessing = false;
  this.username = '';
  this.password = '';
  this.errors = {
    main: '',
    validation: {
      username: false,
      password: false,
    },
  };

  // Shows message if user was redirected due to not being logged in
  this.errors.main = errorIfRedirected() || '';

  // This works on password or username either, input data not disappearing while refreshing
  this.$on('.text-input', 'input', ({ target }) => {
    this[target.name] = target.value;
  });

  // Handling Modal's close btn
  this.$on('.modal__close-btn', 'click', () => {
    this.errors.main = '';
    this.$refresh();
  });

  // Handling form
  this.$on('#login', 'submit', async (e) => {
    e.preventDefault();
    this.isProcessing = true; // changing submit btn text

    const username = e.target.elements.username.value;
    const password = e.target.elements.password.value;

    const validationEffect = validateUser(username, password);
    Object.assign(this, validationEffect);
    this.$refresh();
    if (!this.isProcessing) return; // If isProcessing is false, it means that user wasnt valid

    const loginResponse = await login({ username, password }); // It redirects if succesful
    Object.assign(this, loginResponse);
    this.$refresh();
  });
});

route('/success', success, function () {
  checkIfLogged(); // if token isn't saved, user is being redirected to login panel
  this.$on('.btn', 'click', logout); // logout button - it removes token and redirects to home
});
/*eslint-disable*/
route('*', e404, function () {});
