module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',
  entry: ['./src/app.js'],
  output: {
    filename: './src/app.js',
    path: `${__dirname}/build`,
  },
  devServer: {
    hot: true,
  },
  module: {
    rules: [
      {
        test: /\.(html)$/,
        exclude: /(index)\.html$/,
        use: {
          loader: 'html-loader',
        },
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader', 'postcss-loader'],
      },
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader'],
      },
    ],
  },
};
